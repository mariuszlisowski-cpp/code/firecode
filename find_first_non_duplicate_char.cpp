// find the first non duplicate character in a string
// firecode.io

#include <iostream>
#include <unordered_map>

void displayMap(const std::unordered_map<char, int> &mp) {
    for (auto it : mp)
        std::cout << it.first << " | " << it.second << std::endl;
}

char first_non_repeating(std::string str) {
    std::unordered_map<char, int> mp;
    for (auto it : str)
        mp[it]++;
    for (auto it: str)
        if (mp[it] == 1)
            return it;
    return '0';
}

int main() {
    std::string s{"abcdcd"};

    std::cout << first_non_repeating(s) << std::endl;

    return 0;
}
