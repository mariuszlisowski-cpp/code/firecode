// no twins allowed

#include <algorithm>
#include <iostream>
#include <string>

bool are_all_characters_unique(std::string str) {
    std::sort(std::begin(str), std::end(str));
    return std::unique(std::begin(str), std::end(str)) == str.end();
}

int main() {
    std::string s{ "qwerty" };

    if (are_all_characters_unique(s))
        std::cout << "Unique\n";
    else
        std::cout << "NOT unique\n";

    return 0;
}