// find the missing number

#include <iostream>

int find_missing_number(int arr[], int n) {
    if (n > 1) {
        for (int i = 0; i < n; ++i) {
            if (arr[i] != i + 1)
                return i + 1;
        }
    }
    return 0;
}

int main() {
    int arr[]{ 1, 2, 3, 4, 5, 7, 8 };  // [1..n+1]
    int size = sizeof(arr) / sizeof(*arr);

    std::cout << find_missing_number(arr, size);

    return 0;
}