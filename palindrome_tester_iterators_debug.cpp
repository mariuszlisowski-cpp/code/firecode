// palindrome (debug)
// #iterators #transform

#include <iostream>
#include <algorithm>

bool is_string_palindrome(std::string str) {
    // edge case
    if (str.empty())
        return true;
    // case insensitivness
    std::transform(std::begin(str), std::end(str), std::begin(str), ::tolower);

    auto beg = str.begin();
    auto end = str.end() - 1; 
    do {
        // debug
        std::cout << *beg << " : " << *end << std::endl;

        if (*beg != *end)
            return false;
    } while (++beg < --end);
    return true;
}

int main() {
    std::string s {"abcd I dcba"};

    if (is_string_palindrome(s))
        std::cout << "Panindrome" << std::endl;
    else
        std::cout << "NOT panindrome" << std::endl;

    return 0;
}