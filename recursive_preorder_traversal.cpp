// recursive preorder traversal

#include <iostream>
#include <vector>

struct treeNode {
    int value;
    struct treeNode* left;
    struct treeNode* right;
};

std::vector<int> preordered_list;

void preorder(treeNode* root) {
    if (!root)
        return;
    preordered_list.emplace_back(root->value);
    preorder(root->left);
    preorder(root->right);
}

int main() {
        return 0;
}