// single_number

#include <iostream>
#include <unordered_map>

int single_number(int arr[], int sz) {
    if (sz > 0) {
        std::unordered_map<int, int> map;
        for (int i = 0; i < sz; i++) {
            map[arr[i]]++;
        }
        for (auto&& pair : map) {
            if (pair.second == 1) {
                return pair.first;
            }
        }
    }

    return 0;
}

int main() {
    int array[] { 1, 2, 3, 4, 1, 2, 4, 3, 5 }; // 5
    // int array[] { -1, -1, -1, -1, -5 }; // -5
    // int array[] {}; // 0

    std::cout << single_number(array, sizeof(array) / sizeof(*array)) << std::endl;

    return 0;
}
