// diameter_tree 

#include <iostream>
#include <queue>

struct treeNode{
    int value;
    struct treeNode* left;
    struct treeNode* right;
};

void insert(treeNode** node, int data);
void levelTraversal(treeNode* node);

int height(treeNode* root) {
    if (root == nullptr) {
        return 0;
    }
    
    return std::max(height(root->left), height(root->right)) + 1;
}

int diameter_tree(treeNode* root) {
    if (root == nullptr) {
        return 0;
    }
    int rootDiameter = height(root->left) + height(root->right) + 1;
    int leftDiameter = diameter_tree(root->left);
    int rightDiameter = diameter_tree(root->right);

    return std::max(rootDiameter,
                    std::max(leftDiameter, rightDiameter));
}

int main() {
    treeNode* root{};

    insert(&root, 1);
    insert(&root, 4);
    insert(&root, 2);
    insert(&root, 5);
    insert(&root, 3); // diameter 4
    
    // insert(&root, 20);
    // insert(&root, 16);
    // insert(&root, 14);
    // insert(&root, 18);
    // insert(&root, 10);
    // insert(&root, 15);
    // insert(&root, 17);
    // insert(&root, 19); // diameter 5

    levelTraversal(root);

    std::cout << diameter_tree(root) << std::endl;

    return 0;
}

// pointer passed by a pointer
void insert(treeNode ** node, int data) {
    // dereference a pointer
    if (!*node) {
        *node = new treeNode;
        (*node)->value = data;
        (*node)->left = nullptr;
        (*node)->right = nullptr;
    } else if (data <= (*node)->value) {
        insert(&(*node)->left, data);
    } else {
        insert(&(*node)->right, data);	
    }
}

void levelTraversal(treeNode * root) {
    if (root == nullptr) {
        return;
    }
    std::queue<treeNode*> q;
    q.push(root);
    while (!q.empty()) {
        treeNode* current = q.front();
        std::cout << current->value << ' ';
        if (current->left)
            q.push(current->left);
        if (current->right)
            q.push(current->right);
        q.pop();
    }
    std::cout << std::endl;
}