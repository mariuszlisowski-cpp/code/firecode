// even or odd
// firecode.io

#include <iostream>

class listNode {
public:
    int value;
    listNode * next;
};

listNode* insert_at_head(listNode* head, int data)
{
    listNode * node = new listNode;
    node->value = data;
    if (!head) {
        node->next = nullptr;
        head = node;
    }
    else {
        node->next = head;
        head = node;
    }
    return head;
}

// SOLUTION START
bool is_list_even(listNode* head) {
    int count {};
    while (head) {
        head = head->next;
        count++;
    }
    return !(count % 2);
}
// SOLUTION END

bool is_list_even_original(listNode* head) {   
    listNode* temp = head;
    while (temp && temp->next)
        temp = temp->next->next;
    if (temp == nullptr)
        return true; /* even */
    else
        return false; /* odd */
}

void displayList(listNode * head) {
    while (head) {
        std::cout << head->value << " -> ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    listNode * head = nullptr;

    head = insert_at_head(head, 10);
    head = insert_at_head(head, 20);
    // head = insert_at_head(head, 30);
    displayList(head);

    if (is_list_even_original(head))
        std::cout << "Even" << std::endl;
    else
        std::cout << "Odd" << std::endl;

    displayList(head);

    return 0;
}
