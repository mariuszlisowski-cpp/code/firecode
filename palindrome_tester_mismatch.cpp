// palindrome
// #mismatch()

#include <iostream>
#include <algorithm>

bool is_string_palindrome(std::string str) {
    auto it = std::mismatch(str.begin(), str.end(), str.rbegin());
    return it.first == str.end();
}

int main() {
    std::string s {"rotator"};

    if (is_string_palindrome(s))
        std::cout << "Panindrome" << std::endl;
    else
        std::cout << "NOT panindrome" << std::endl;

    return 0;
}