// pow!
// firecode.io

#include <iostream>

double power(double x, int n) {
    if (!x)
        return 0;
    if (!n)
        return 1;
    if (n == 1)
        return x;
    if (n < 0)
        return 1 / power(x, -n);
    return x * power(x, n - 1);
}

int main() {
    std::cout << power(-2, -3);

    return 0;
}
