// reverse_list

#include <iostream>
#include <stack>

struct listNode {
    int value;
    listNode* next;

    listNode(int data) : value(data), next(nullptr) {}
};

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
listNode* reverse_list(listNode* head) {
    if (!head) {
        return nullptr;
    }
    listNode* temp = head;
    std::stack<listNode*> nodes{};
    while (temp) {
        nodes.push(temp);
        temp = temp->next;
    }
    temp = head = nodes.top();
    nodes.pop();
    while (!nodes.empty()) {
        temp->next = nodes.top();
        nodes.pop();
        temp = temp->next;
    }
    temp->next = nullptr;

    return head;
}
////////////////////////////////////////////////////////////////////////////////

void insertNodeAtTail(listNode* &head, int data) {
    listNode* node = new listNode(data);
    if (head) {
        listNode* temp = head;
        while (temp->next) {
            temp = temp->next;
        }
        temp->next = node;
    } else {
        head = node;
    }
}

void displayNodes(listNode* head) {
    while (head) {
        std::cout << head->value << " - > ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    listNode* head{};

    insertNodeAtTail(head, 1);
    insertNodeAtTail(head, 2);
    insertNodeAtTail(head, 3);
    insertNodeAtTail(head, 4);
    insertNodeAtTail(head, 5);
    insertNodeAtTail(head, 6);
    displayNodes(head);

    head = reverse_list(head);
    displayNodes(head);

    return 0;
}
