// find the number that appears once
// firecode.io

#include <iostream>
#include <unordered_map>

int single_number(int arr[], int sz) {
    std::unordered_map<int, int> singles;
    if (sz != 0) {
        for (int i = 0; i < sz; i++) {
            ++singles[arr[i]];
        }
        for (int i = 0; i < sz; i++) {
            if (singles[arr[i]] == 1) {
                return arr[i];
            }
        }
    }

    return 0;
}

int main() {
    int array[] {4, 5, 5, 4, 4, 3, 3, 2, 2, 1};
    int size = sizeof(array) / sizeof(*array);

    std::cout << single_number(array, size) << std::endl;

    return 0;
}
