// find the Nth node from the end of a linked list without using extra space 

#include <iostream>

struct listNode {
    int value;
    listNode* next;

    listNode(int data) : value(data), next(nullptr) {}
};

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
listNode* find_middle_node(listNode* head) {
    if (!head) { return nullptr; }
    listNode* temp = head;
    int counter{};
    while (temp) {
        temp = temp->next;
        ++counter;
    }
    temp = head;
    int middle = counter % 2 ? counter / 2 : counter / 2 - 1;
    while (middle--) {
        temp = temp->next;
    }

    return temp;
}
////////////////////////////////////////////////////////////////////////////////

void insertNodeAtTail(listNode* &head, int data) {
    listNode* node = new listNode(data);
    if (head) {
        listNode* temp = head;
        while (temp->next) {
            temp = temp->next;
        }
        temp->next = node;
    } else {
        head = node;
    }
}

void displayNodes(listNode* head) {
    while (head) {
        std::cout << head->value << " - > ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    listNode* head{};

    insertNodeAtTail(head, 1);
    insertNodeAtTail(head, 2);
    insertNodeAtTail(head, 3);
    insertNodeAtTail(head, 4);
    insertNodeAtTail(head, 5);
    insertNodeAtTail(head, 6);
    displayNodes(head);

    listNode* foundNode = find_middle_node(head);
    if (foundNode) {
        std::cout << foundNode->value << std::endl;
    } else {
        std::cout << "nullptr" << std::endl;
    }
    return 0;
}
