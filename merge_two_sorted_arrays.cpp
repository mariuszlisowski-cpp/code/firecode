// merge two sorted arrays
// firecode.io

#include <iostream>
#include <algorithm>

int* merge(int arr_left[],int sz_left, int arr_right[], int sz_right){
    int* arr_merged = new int [sz_left+sz_right];
    std::merge(arr_left, arr_left + sz_left, arr_right, arr_right + sz_right, arr_merged);
    return arr_merged;
}

int main() {
    int arrayA[] {2};
    int arrayB[] {2};
    int sizeA = sizeof(arrayA) / sizeof(*arrayA);
    int sizeB = sizeof(arrayB) / sizeof(*arrayB);

    int * arrayMerged = merge(arrayA, sizeA, arrayB, sizeB);

    for (int i = 0; i < sizeA + sizeB; ++i)
        std::cout << arrayMerged[i] << " | ";
    std::cout << std::endl;

    return 0;
}
