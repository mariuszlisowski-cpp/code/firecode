// max gain
// firecode.io

#include <iostream>
#include <algorithm>

int max_gain(int arr[], int sz) {
    int max = *std::max_element(arr, arr + sz);
    for (int i = 0; i < sz; ++i)
        if (arr[i] == 0)
            return max;
    return 0;
}

int main() {
    int array[] {0, 50, 10, 100, 30};
    int size = sizeof(array) / sizeof(*array);

    std::cout << max_gain(array, size) << std::endl;

    return 0;
}
