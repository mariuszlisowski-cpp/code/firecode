// find the number that appears once
// firecode.io

#include <iostream>
#include <unordered_set>

int single_number(int arr[], int sz) {
    if (!sz)
        return 0;
    std::unordered_set<int> set;
    for (int i = 0; i < sz; ++i) {
        if (set.find(arr[i]) == set.end())
            set.insert(arr[i]);
        else
            set.erase(arr[i]);
    }
    return *(set.begin());
}

int main() {
    int array[] {5, 5, 4, 4, 3, 3, 2, 2, 1};
    int size = sizeof(array) / sizeof(*array);

    std::cout << single_number(array, size) << std::endl;

    return 0;
}
