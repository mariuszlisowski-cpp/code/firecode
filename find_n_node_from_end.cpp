// find the Nth node from the end of a linked list without using extra space 

#include <iostream>

struct listNode {
    int data_;
    listNode* next;

    listNode(int data) : data_(data), next(nullptr) {}
};

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
listNode* find_n_node_from_end(listNode* head, int n) {
    if (!head) {
        return nullptr;
    }
    listNode* first = head;
    listNode* second = head;
    while (n--) {
        second = second->next;
        if (!second && !n) {
            return head;
        }
        if (!second) {
            return nullptr;
        }
    }
    while (second) {
        second = second->next;
        first = first->next;
    }
    
    return first;
}
////////////////////////////////////////////////////////////////////////////////

void insertNodeAtTail(listNode* &head, int data) {
    listNode* node = new listNode(data);
    if (head) {
        listNode* temp = head;
        while (temp->next) {
            temp = temp->next;
        }
        temp->next = node;
    } else {
        head = node;
    }
}

void displayNodes(listNode* head) {
    while (head) {
        std::cout << head->data_ << " - > ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    listNode* head{};

    insertNodeAtTail(head, 1);
    insertNodeAtTail(head, 2);
    insertNodeAtTail(head, 3);
    insertNodeAtTail(head, 4);
    insertNodeAtTail(head, 5);
    insertNodeAtTail(head, 6);
    displayNodes(head);

    listNode* foundNode = find_n_node_from_end(head, 2);
    if (foundNode) {
        std::cout << foundNode->data_ << std::endl;
    } else {
        std::cout << "nullptr" << std::endl;
    }
    return 0;
}
