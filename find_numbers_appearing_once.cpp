// find the numbers that appear once

#include <iostream>
#include <unordered_map>
#include <unordered_set>

std::unordered_set<int> single_numbers(int arr[], int sz) {
    std::unordered_set<int> singles;
    std::unordered_map<int, int> map;
    if (sz != 0) {
        for (int i = 0; i < sz; i++) {
            ++map[arr[i]];
        }
        for (int i = 0; i < sz; i++) {
            if (map[arr[i]] == 1) {
                singles.insert(arr[i]);
            }
        }
    }

    return singles;
}

int main() {
    int array[] {9, 4, 5, 5, 4, 4, 3, 3, 2, 2, 1};
    int size = sizeof(array) / sizeof(*array);

    auto singles = single_numbers(array, size);

    for (auto&& value : singles) {
        std::cout << value << ' ';
    }
    
    return 0;
}
