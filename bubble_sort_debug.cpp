// 

#include <iostream>

int* bubble_sort_array(int arr[], int size){
    bool swapped;
    do {
        swapped = false;
        for (int i = 0; i < size - 1; ++i) {
            // debug
            std::cout << arr[i] << " : " << arr[i + 1];
            
            if (arr[i] > arr[i + 1]) {
                // swapping
                int temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
                swapped = true;
                // debug
                std::cout << " - swapped" << std::endl;
            }
            // debug
            else { 
                std::cout << " - no swap" << std::endl;
            }
        }
        // debug
        if (swapped)
            std::cout << "Swap occured" << std::endl;
        else 
            std::cout << "No swap - exitting" << std::endl;
        
        --size;
        // debug
        std::cout << "Unsorted left: " << size << std::endl << std::endl;
    } while (swapped);


    return arr;
}

int main() {
    int arr[] {5, 6, 3, 8, 6, 2};
    int size = sizeof(arr) / sizeof(*arr);

    bubble_sort_array(arr, size);

    for (int i = 0; i < 6; ++i)
        std::cout << arr[i] << " ";	
    
    return 0;
}