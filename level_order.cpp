// level_order

#include <iostream>
#include <queue>
#include <vector>

struct treeNode{
    int value;
    struct treeNode* left;
    struct treeNode* right;
};

treeNode * createNewNode(int value) {
    treeNode * node = new treeNode;
    node->value = value;
    node->left = nullptr;
    node->right = nullptr;
    return node;
}

treeNode * insert(treeNode * node, int value) {
    if (!node)
        node = createNewNode(value);
    else if (value <= node->value)
        node->left = insert(node->left, value);
    else
        node->right = insert(node->right, value);
    return node;
}

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
std::vector<int> level_order(treeNode* root) {
    std::vector<int> nodes{};
    if (!root)
        return nodes;
    std::queue<treeNode*> q;
    q.push(root);
    while (!q.empty()) {
        treeNode* current = q.front();
        nodes.emplace_back(current->value);
        if (current->left)
            q.push(current->left);
        if (current->right)
            q.push(current->right);
        q.pop();
    }
    
    return nodes;
}
////////////////////////////////////////////////////////////////////////////////

int main() {
    treeNode* root{};

    root = insert(root, 1);
    root = insert(root, 2);
    root = insert(root, 3);
    root = insert(root, 4);
    root = insert(root, 5);
    root = insert(root, 6);
    root = insert(root, 7);

    std::vector<int> nodes = level_order(root);
    if (!nodes.empty()) {
        for (auto &&i : nodes) {
            std::cout << i << ' ';
        }
        std::cout << std::endl;
    }

    return 0;
}
