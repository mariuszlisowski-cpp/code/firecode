// palindrome
// #iterators

#include <algorithm>
#include <iostream>

bool is_string_palindrome(std::string str) {
    if (!str.empty()) {
        auto beg = str.begin();
        auto end = str.end() - 1;
        do {
            if (*beg != *end)
                return false;
        } while (++beg < --end);
    }
    return true;
}

int main() {
    std::string s{ "Rotator" };

    if (is_string_palindrome(s))
        std::cout << "Panindrome" << std::endl;
    else
        std::cout << "NOT panindrome" << std::endl;

    return 0;
}