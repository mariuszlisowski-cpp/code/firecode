// insert node at tail (initialization)

#include <iostream>

struct listNode {
public:
    int value;
    listNode * next;
};

listNode* insert_at_tail(listNode* head, int data) {
    if (!head)
        return new listNode{data, nullptr};
    else {
        listNode* last = head;
        while (last->next)
            last = last->next;
        last->next = new listNode{data, nullptr};
    }
    return head;
}

void displayList(listNode * head) {
    while (head) {
        std::cout << head->value << " -> ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    listNode * head = nullptr;

    head = insert_at_tail(head, 10);
    head = insert_at_tail(head, 20);
    head = insert_at_tail(head, 30);

    displayList(head);

    return 0;
}
