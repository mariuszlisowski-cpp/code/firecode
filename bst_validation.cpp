// BST valildation

#include <iostream>
#include <vector>
#include <algorithm>

struct treeNode{
    int value;                  /* value of the node */
    struct treeNode* left;      /* left pointer to the left child node */
    struct treeNode* right;     /* right pointer to the right child node */
};

treeNode * insert_node(treeNode* node, int data) {
    if (!node) {
        node = new treeNode;
        node->value = data;
        node->left = nullptr;
        node->right = nullptr;
    }
    else if (data <= node->value)
        node->left = insert_node(node->left, data);
    else {
        node->right = insert_node(node->right, data);
    }
    return node;
}
/// SOLUTION START  //////////////////////////////////////////////////////////
// prepare sorted array 
void inorder_traversal(treeNode * node, std::vector<int>& v) {
    if (!node)
        return;
    inorder_traversal(node->left, v);
    v.push_back(node->value);
    inorder_traversal(node->right, v);
}
// original firecode solution
bool is_bst_1(treeNode* root, int min, int max) {
    if (root==NULL) // an empty tree is a BST
        return true;   
    // vheck if this node violates the min/max constraint
    if (root->value< min || root->value > max)
        return false;
    // validate the subtrees recursively, adjusting the min or max values
    // left subtree must be <= root->value
    // right subtree must be > root->value
    return is_bst_1(root->left, min, root->value - 1) &&  
           is_bst_1(root->right, root->value + 1, max);
}
// yt mycodersschool solution
bool is_bst_2(treeNode* node, int minVal, int maxVal) {
    if (!node)
        return true;
    if (node->value > minVal &&
        node->value < maxVal &&
        is_bst_2(node->left, minVal, node->value) &&
        is_bst_2(node->right, node->value, maxVal))
        return true;
    else
        return false;
}
// main validate
bool validate_bst(treeNode* root) {
    /* 1st APPROACH: comparing the range */
    return is_bst_2(root, INT_MIN, INT_MAX);

    /* 2nd APPROACH: checking if sorted */
    // if (!root)
    // 	return false;
    // std::vector<int> vec, test;
    // inorder_traversal(root, vec);
    // test = vec;
    // std::sort(test.begin(), test.end());
    // if (vec == test)
    // 	return true;
    // return false;
}
/// SOLUTION END ////////////////////////////////////////////////////////////

int main() {
    treeNode* root = nullptr;

    root = insert_node(root, 15);
    root = insert_node(root, 10);
    root = insert_node(root, -8);
    root = insert_node(root, 12);
    root = insert_node(root, 20);
    root = insert_node(root, 17);
    root = insert_node(root, 25);
        
    std::vector<int> v;
    inorder_traversal(root, v);

    for (const auto& e : v)
        std::cout << e << " ";
    std::cout << std::endl;
    
    if (validate_bst(root))
        std::cout << "BST tree" << std::endl;
    else
        std::cout << "NOT a BST tree" << std::endl;


    return 0;
}