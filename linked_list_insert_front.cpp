// insert a node at the front of a linked list
// firecode.io

#include <iostream>

class listNode {
public:
    int value;
    listNode * next;
};

// SOLUTION START
listNode* insert_at_head(listNode* head, int data)
{
    listNode * node = new listNode;
    node->value = data;
    if (!head) {
        node->next = nullptr;
        head = node;
    }
    else {
        node->next = head;
        head = node;
    }
    return head;
}
// SOLUTION END


void displayList(listNode * head) {
    while (head) {
        std::cout << head->value << " -> ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    listNode * head = nullptr;

    head = insert_at_head(head, 10);
    head = insert_at_head(head, 20);
    head = insert_at_head(head, 30);
    displayList(head);

    return 0;
}
