// Count Paths on a Game Board
// Firecode.io

#include <iostream>

int count_paths(int rows, int cols) {
    if(rows == 0 || cols == 0) {
        return 0;
    }
    if(rows == 1 || cols == 1) {
        return 1;
    }
    int **memo = new int*[rows];
    for (int i = 0; i < rows; i++) 
        memo[i] = new int[cols];
    for(int  i = 0; i < rows; i++) {
        memo[i][0] = 1;
    }
    for(int j = 0; j < cols; j++) {
        memo[0][j] = 1;
    }
    for(int i = 1; i < rows; i++){
        for(int j = 1; j < cols; j++){
            memo[i][j] = memo[i-1][j] + memo[i][j-1];
        }
    }
    int result = memo[rows-1][cols-1];
    for (int i = 0; i < rows; i++) {
       delete [] memo[i];
    } 
    delete [] memo;
    
    return result;
}

int count_paths_recursive(int rows, int cols) {
    if (rows == 1 || cols == 1) {
        return 1; 
    }
    return count_paths(rows - 1, cols) + count_paths(rows, cols - 1);
}

int main() {
    std::cout << count_paths(15, 16) << std::endl; // 77558760
    std::cout << count_paths(12, 10) << std::endl; // 167960
    std::cout << count_paths(12, 6) << std::endl; // 4368

    std::cout << count_paths(5, 3) << std::endl; // 15
    std::cout << count_paths(3, 5) << std::endl; // 15

    std::cout << count_paths(6, 4) << std::endl; // 56
    std::cout << count_paths(4, 6) << std::endl; // 56

    return 0;
}
