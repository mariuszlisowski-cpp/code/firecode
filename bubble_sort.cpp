// bubble sort
// #for_loop #while_loop

#include <iostream>

// while loop (iterates till any swap)
int* bubble_sort_array_1(int arr[], int size) {
    bool swapped;
    do {
        swapped = false;
        for (int i = 0; i < size - 1; ++i) {
            if (arr[i] > arr[i + 1]) {
                int temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
                swapped = true;
            }
        }
        --size;
    } while (swapped);
    return arr;
}

// for loop (iterates all elements)
int* bubble_sort_array_2(int arr[], int size) {
    // no need to sort
    if (size <= 1)
        return arr;
    // sorting
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size - 1 - i; ++j) {
            if (arr[j] > arr[j + 1]) {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    return arr;
}

int main() {
    int arr[] {5, 6, 3, 8, 6, 2};
    int size = sizeof(arr) / sizeof(*arr);

    bubble_sort_array_2(arr, size);

    for (int i = 0; i < 6; ++i)
        std::cout << arr[i] << " ";	
    
    return 0;
}