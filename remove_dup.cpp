// remove_dup 

#include <array>
#include <algorithm>
#include <iostream>

void printArray(int array[], int size) {
    if (array != nullptr) {
        for (int i = 0; i < size; i++) {
            std::cout << array[i] << ' ';
        }
    } else {
        std::cout << "NULL";
    }
    std::cout << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
int* remove_dup(int A[], int sz) {
    /* Alocate the memory of Output array of maximum size n. 
    Fill the new array with unique elements, which have duplicates entries in input array 
    Size of Output array will be less than n. */
    int *output = NULL;
    // Add your code below this line. Do not modify any other code.

    output = new int[sz]{};
    std::sort(A, A + sz);

    // VERBOSE:
    printArray(A, sz);

    int j{};
    bool repeated = false;
    for (int i = 0; i < sz - 1; i++) {
        if (A[i] == A[i+1] && repeated == false) {
            repeated = true;
            output[j++] = A[i];
        } else {
            repeated = false;
        }
    }
    if (j == 0) {
        return nullptr;
    }
    
    // Add your code above this line. Do not modify any other code.
    return output;
    
}
////////////////////////////////////////////////////////////////////////////////

int main() {
    // int arr[] { 1, 2, 4, 4, 5, 6, 2, 1 };    // 1, 2, 4
    // int arr[] { 1, 2, 4, 5, 6 };             // null
    // int arr[] { 3, 1, 2, 4, 1 };             // 1
    // int arr[] { 1 };                         // null
    int arr[] {1, 4, 5, 4, 4, 5, 5, 3};      // 4, 5

    int size = sizeof(arr) / sizeof(*arr);
    std::cout << "size: " << size << std::endl;
    
    int* output = remove_dup(arr, size);
    printArray(output, size);

    return 0;
}
