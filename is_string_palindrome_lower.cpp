// 

#include <algorithm>
#include <iostream>
#include <string>

bool is_string_palindrome(std::string str) {
    std::transform(str.begin(), str.end(),
                   str.begin(),
                   ::tolower);
    std::string temp = str;
    std::reverse(str.begin(), str.end());

    return temp == str;
}

int main() {
    std::string str = "";
    std::cout << (is_string_palindrome(str) ? "palindrome" : "not palindrome") << std::endl;

    return 0;
}