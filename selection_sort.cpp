// selection sort
// firecode.io

#include <iostream>

int* selection_sort_array(int arr[], int size) {
    int minIndex, min {};
    do {
        minIndex = min;
        for (int i = min + 1; i < size; ++i) {
            if (arr[minIndex] > arr[i])
                minIndex = i;
        }
        std::swap(arr[min++], arr[minIndex]);
    } while (min < size - 1);
    return arr;
}

int main() {
    int arr[] { 45, 25, 87, 36, 15, 33, 10 };
    int arrSize =  sizeof(arr) / sizeof(*arr);

    selection_sort_array(arr, arrSize);

    for (int i = 0; i < arrSize; ++i)
        std::cout << arr[i] << ' ';

    return 0;
}
