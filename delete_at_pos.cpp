// delete_at_pos

#include <iostream>

struct listNode {
    int value;
    listNode* next;

    listNode(int data) : value(data), next(nullptr) {}
};

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
listNode* delete_at_pos(listNode* head, int n) {
    if (!head) {
        return nullptr;
    }
    listNode* temp = head;
    listNode* prev = head;
    if (n == 1) {
        head = temp->next;
    }
    while (temp->next && n > 1) {
        prev = temp;
        temp = temp->next;
        n--;
    }
    if (n == 1) {
        prev->next = temp->next;
        delete temp;
    }

    return head;
}
////////////////////////////////////////////////////////////////////////////////

void insertNodeAtTail(listNode* &head, int data) {
    listNode* node = new listNode(data);
    if (head) {
        listNode* temp = head;
        while (temp->next) {
            temp = temp->next;
        }
        temp->next = node;
    } else {
        head = node;
    }
}

void displayNodes(listNode* head) {
    while (head) {
        std::cout << head->value << " - > ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    listNode* head{};

    insertNodeAtTail(head, 1);
    insertNodeAtTail(head, 2);
    insertNodeAtTail(head, 3);
    insertNodeAtTail(head, 4);
    insertNodeAtTail(head, 5);
    insertNodeAtTail(head, 6);
    displayNodes(head);

    head = delete_at_pos(head, 3); // start index 1
    displayNodes(head);

    return 0;
}
