// find the Nth node from the end of a linked list without using extra space 

#include <iostream>

struct listNode {
    int value;
    listNode* next;
};

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
listNode* insert_at_position(listNode* head, int data, int position) {
    listNode* node = new listNode();
    node->value = data;
    node->next = nullptr;
    if (!head) {
        head = node;
    } else if (position == 1) {
        node->next = head;
        head = node;
    }
    // position NOT secured below
    else {
        int counter{};
        listNode* prev = head;
        listNode* next = head->next;
        while (counter < position - 2) {
            prev = prev->next;
            next = prev->next;
            ++counter;
        }
        prev->next = node;
        node->next = next;
    }
    return head;
}
////////////////////////////////////////////////////////////////////////////////

void insertNodeAtTail(listNode* &head, int data) {
    listNode* node = new listNode();
    node->value = data;
    node->next = nullptr;
    if (head) {
        listNode* temp = head;
        while (temp->next) {
            temp = temp->next;
        }
        temp->next = node;
    } else {
        head = node;
    }
}

void displayNodes(listNode* head) {
    while (head) {
        std::cout << head->value << " - > ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    listNode* head{};

    insertNodeAtTail(head, 1);
    insertNodeAtTail(head, 2);
    insertNodeAtTail(head, 3);
    insertNodeAtTail(head, 4);
    insertNodeAtTail(head, 5);
    insertNodeAtTail(head, 6);
    displayNodes(head);

    head = insert_at_position(head, 99, 8);
    displayNodes(head);

    return 0;
}
