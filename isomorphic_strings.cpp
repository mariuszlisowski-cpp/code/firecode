// isomorphic strings
// firecode.io

#include <iostream>
#include <unordered_map>
#include <unordered_set>

bool is_isomorphic(std::string input1, std::string input2) {
    // not equal strings cannot be isomorphic
    if (input1.size() != input2.size())
        return false;
    // empty strings are isomorphic
    if (input1.empty() && input2.empty())
        return true;
    // mapping all unique characters to each other
    std::unordered_map<char, char> iso;
    std::unordered_set<char> set1;
    std::unordered_set<char> set2;
    auto ch1 = input1.begin();
    auto ch2 = input2.begin();
    for (ch1; ch1 != input1.end(); ++ch1, ++ch2)
        if (set1.find(*ch1) == set1.end() && set2.find(*ch2) == set2.end()) {
            set1.insert(*ch1);
            set2.insert(*ch2);
            iso.insert({*ch1, *ch2});
        }
    // creating correctly mapped string
    std::string output;
    for (auto ch : input1) {
        auto it = iso.find(ch);
        if (it != iso.end())
            output += it->second;
    }
    // comparing if the second string is the same as mapped
    if (output != input2)
        return false;
    return true;
}

int main() {
    // not isomorphic
    std::string str1 = "abcz";
    std::string str2 = "zbca";
    // isomorphic
    std::string str3 = "abcabc";
    std::string str4 = "xyzxyz";
    // not isomorphic
    std::string str5 = "abcabc";
    std::string str6 = "xyzxyy";
    // ispotropic
    std::string str7 = "abacdea";
    std::string str8 = "zbzcdvz";

    if (is_isomorphic(str1, str2))
        std::cout << "Isomorphic" << std::endl;
    else
        std::cout << "NOT isomorphic" << std::endl;

    return 0;
}
