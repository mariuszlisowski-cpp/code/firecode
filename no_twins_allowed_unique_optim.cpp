// no twins allowed
// firecode.io

#include <iostream>
#include <algorithm>

// SOLUTION START
bool are_all_characters_unique(std::string str) {
   if (str.empty())
        return true;
    std::sort(str.begin(), str.end());
    return (std::unique(str.begin(), str.end()) != str.end());
}
// SOLUTION END

int main() {
    std::string s {"01234567890"};
    if (are_all_characters_unique(s))
        std::cout << "No duplicates" << std::endl;
    else
        std::cout << "Duplicates found!" << std::endl;


    return 0;
}
